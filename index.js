// console.log("hello world")


// Login Simulation Start
let username;
let password;
let role;

function returnUsername(){
	let getUsername = prompt("Enter your username: ");
	return getUsername;
}

function returnPassword(){
	let getPassword = prompt("Enter your password: ");
	return getPassword;
}

function returnRole(){
	let getRole = prompt("Enter your role: ");
	return getRole;
}

username = returnUsername();
password = returnPassword();
role = returnRole().toLowerCase();

if (username === null) {
	alert("Input must not be empty.")
} else if (username === "") {
	alert("Input must not be empty.")
} else {
	switch (role){
		case 'admin':
			alert("Welcome back to the class portal, admin!");
			break;
		case 'teacher':
			alert("Thank you for logging in, teacher!");
			break;
		case 'student':
			alert("Welcome to the class portal, student!");
			break;

		default:
			alert("Role out of range.");
			break;
	}
}
// Login Simulation End

// Computation of Average Start
function checkAverage(grade1, grade2, grade3, grade4){
	let getAverage = Math.round((grade1 + grade2 + grade3 + grade4) / 4);
	if (getAverage <= 74) {
		console.log(" Hello, " + username + " your average is " + getAverage + ". The letter equivalent is F");
	} else if (getAverage <= 79) {
		console.log(" Hello, " + username + " your average is " + getAverage + ". The letter equivalent is D");
	} else if (getAverage <= 84) {
		console.log(" Hello, " + username + " your average is " + getAverage + ". The letter equivalent is C");
	} else if (getAverage <= 89) {
		console.log(" Hello, " + username + " your average is " + getAverage + ". The letter equivalent is B");
	} else if (getAverage <= 95) {
		console.log(" Hello, " + username + " your average is " + getAverage + ". The letter equivalent is A");
	} else if (getAverage > 95) {
		console.log(" Hello, " + username + " your average is " + getAverage + ". The letter equivalent is A+");
	}
}

let average = checkAverage(96, 96, 96, 95);
// Computation of Average End